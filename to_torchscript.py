import torch

from model import DigitModel

ckpt = 'checkpoints/2023-06-09_11-25/ckpt-epoch=49-val_acc=0.974.ckpt'
outpath = f'models/{ckpt.split("/")[-2]}_{".".join(ckpt.split("/")[-1].split(".")[:-1])}.pt'

model = DigitModel.load_from_checkpoint(ckpt)
script = model.to_torchscript()

torch.jit.save(script, outpath)
