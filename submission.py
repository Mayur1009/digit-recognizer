import torch
import pandas as pd
import numpy as np
import torchvision.transforms as T
from torch.utils.data import Dataset, random_split, DataLoader
from tqdm import tqdm
from model import DigitModel


class DigitTest(Dataset):
    def __init__(self, csv_path, transforms=None):
        super().__init__()
        self.csv_path = csv_path
        self.transforms = transforms
        self.db = pd.read_csv(self.csv_path)

    def __len__(self):
        return self.db.shape[0]

    def __getitem__(self, index):
        rv = self.db.iloc[index]
        # label = torch.tensor(rv['label'])
        # one_hot_label = torch.nn.functional.one_hot(label, num_classes=10).to(torch.float)
        img = rv.to_numpy(dtype=np.float32).reshape((28, 28, 1))
        img = img / 255.0
        img = T.ToTensor()(img)
        if self.transforms:
            img = self.transforms(img)
        return img


if __name__ == '__main__':
    subdf = pd.read_csv('data/sample_submission.csv')

    dataset = DigitTest('data/test.csv')
    test_loader = DataLoader(dataset)

    model = torch.jit.load('models/2023-06-09_11-25_ckpt-epoch=49-val_acc=0.974.pt')
    model.to('cuda')
    model.eval()

    preds = []
    for i, img in tqdm(enumerate(dataset)):
        pred = int(model(img.to('cuda').unsqueeze(0)).argmax().to('cpu'))
        preds.append(pred)

    subdf['Label'] = preds
    subdf.to_csv('data/subm_1.csv', index=False)
