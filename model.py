import torch
import torch.nn as nn
import pytorch_lightning as pl
import torchmetrics.classification as tmc
from torch.optim import SGD


class DigitModel(pl.LightningModule):
    def __init__(self, lr=0.1):
        super().__init__()
        self.save_hyperparameters()

        self.input_size = (28, 28)
        self.example_input_array = torch.randn((1, 1, *self.input_size))
        self.lr = lr

        self.loss = nn.CrossEntropyLoss()
        self.acc = tmc.MulticlassAccuracy(num_classes=10)
        self.pre = tmc.MulticlassPrecision(num_classes=10)
        self.rec = tmc.MulticlassRecall(num_classes=10)
        self.f1 = tmc.MulticlassF1Score(num_classes=10)

        self.conv1 = nn.Conv2d(in_channels=1, out_channels=16, kernel_size=5)
        self.relu1 = nn.ReLU()
        self.maxpool1 = nn.MaxPool2d(kernel_size=2)
        self.conv2 = nn.Conv2d(in_channels=16, out_channels=32, kernel_size=5)
        self.relu2 = nn.ReLU()
        self.maxpool2 = nn.MaxPool2d(kernel_size=2)

        infeat = self._features(torch.randn(1, 1, *self.input_size)).shape[-1]

        self.fc1 = nn.Linear(infeat, 128)
        self.relu3 = nn.ReLU()
        self.dropout1 = nn.Dropout(0.2)
        self.fc2 = nn.Linear(128, 10)
        # self.softmax = nn.Softmax()

    def _features(self, x):
        out = self.conv1(x)
        out = self.relu1(out)
        out = self.maxpool1(out)
        out = self.conv2(out)
        out = self.relu2(out)
        out = self.maxpool2(out)
        return out.view(out.size(0), -1)

    def forward(self, x):
        features = self._features(x)
        out = self.fc1(features)
        out = self.relu3(out)
        out = self.dropout1(out)
        out = self.fc2(out)
        # out = self.softmax(out)
        return out

    def _get_loss(self, batch):
        x, y = batch
        out = self(x)
        loss = self.loss(out, y)
        return out, loss

    def _metrics(self, true, pred):
        acc = self.acc(pred, true)
        pre = self.pre(pred, true)
        rec = self.rec(pred, true)
        f1 = self.f1(pred, true)
        return acc, pre, rec, f1

    def training_step(self, batch, batch_idx):
        x, y = batch
        out, loss = self._get_loss(batch)
        acc, pre, rec, f1 = self._metrics(y, out)
        self.log('train_loss', loss.item(), on_step=True, on_epoch=True, prog_bar=True)
        self.log('train_acc', acc, on_step=True, on_epoch=True, prog_bar=True)
        self.log('train_precision', pre, on_step=True, on_epoch=True, prog_bar=True)
        self.log('train_recall', rec, on_step=True, on_epoch=True, prog_bar=True)
        self.log('train_f1score', f1, on_step=True, on_epoch=True)
        return loss

    def validation_step(self, batch, batch_idx):
        x, y = batch
        out, loss = self._get_loss(batch)
        acc, pre, rec, f1 = self._metrics(y, out)
        self.log('val_loss', loss.item(), prog_bar=True)
        self.log('val_acc', acc, prog_bar=True)
        self.log('val_precision', pre, prog_bar=True)
        self.log('val_recall', rec, prog_bar=True)
        self.log('val_f1score', f1)
        return loss

    def configure_optimizers(self):
        optimizer = SGD(self.parameters(), lr=self.lr)
        return {"optimizer": optimizer}  # , "lr_scheduler": scheduler, "monitor": "val_acc"}
