import torch
import pytorch_lightning as pl
import pytorch_lightning.callbacks as plc
from pytorch_lightning.loggers import WandbLogger
from datetime import datetime
from dataset import DigitDataset
from model import DigitModel


if __name__ == '__main__':
    torch.set_float32_matmul_precision('high')
    torch.backends.cudnn.enabled = True     # type: ignore
    torch.backends.cudnn.benchmark = True   # type: ignore
    pl.seed_everything(10)

    datamodule = DigitDataset(train_csv='data/train.csv', batch_size=512)
    model = DigitModel()

    logger = WandbLogger(
        project='Digit-Recognizer',
        name=f'{datetime.now().strftime("%Y-%m-%d_%H-%M")}',
        log_model='all'
    )
    logger.watch(model, log='all', log_graph=True)
    logger.log_graph(model, torch.randn((1, 28, 28)))

    trainer = pl.Trainer(
        accelerator='gpu', max_epochs=50,
        callbacks=[
            plc.RichProgressBar(leave=True),
            plc.ModelCheckpoint(
                monitor="val_acc", mode="max",
                dirpath=f'checkpoints/{datetime.now().strftime("%Y-%m-%d_%H-%M")}/',
                save_last=True, filename='ckpt-{epoch}-{val_acc:.3f}', every_n_epochs=1, save_top_k=3
            ),
            plc.LearningRateMonitor(),
            # plc.EarlyStopping(monitor="val_acc", patience=10, mode="max", check_finite=True),
        ],
        logger=logger
    )

    print('Training....')
    trainer.fit(model, datamodule=datamodule)
    # print('Testing....')
    # trainer.test(model, datamodule=datamodule, ckpt_path='best')

