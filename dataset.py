import pandas as pd
import numpy as np
import torch
import torchvision.transforms as T  # type: ignore
import pytorch_lightning as pl
from torch.utils.data import Dataset, random_split, DataLoader


class Digit(Dataset):
    def __init__(self, csv_path, transforms=None):
        super().__init__()
        self.csv_path = csv_path
        self.transforms = transforms
        self.db = pd.read_csv(self.csv_path)

    def __len__(self):
        return self.db.shape[0]

    def __getitem__(self, index):
        rv = self.db.iloc[index]
        label = torch.tensor(rv['label'])
        one_hot_label = torch.nn.functional.one_hot(label, num_classes=10).to(torch.float)
        img = rv.drop('label').to_numpy(dtype=np.float32).reshape((28, 28, 1))
        img = img / 255.0
        img = T.ToTensor()(img)
        if self.transforms:
            img = self.transforms(img)
        return img, label


class DigitDataset(pl.LightningDataModule):
    def __init__(self, train_csv, transforms=None, batch_size=128, num_workers=4, pin_memory=True):
        super().__init__()
        self.train_path = train_csv
        if transforms is None:
            self.transforms = T.Compose([T.RandomRotation(10), T.RandomCrop((28, 28)), T.RandomPerspective()])
        else:
            self.transforms = transforms

        self.batch_size = batch_size
        self.num_workers = num_workers
        self.pin_m = pin_memory

    def setup(self, stage):
        dataset = Digit(self.train_path, self.transforms)
        self.train_ds, self.val_ds = random_split(dataset, [0.8, 0.2])

    def train_dataloader(self):
        return DataLoader(self.train_ds, self.batch_size, shuffle=True, num_workers=self.num_workers,
                          pin_memory=self.pin_m)

    def val_dataloader(self):
        return DataLoader(
            self.val_ds, self.batch_size, num_workers=self.num_workers, pin_memory=self.pin_m
        )
