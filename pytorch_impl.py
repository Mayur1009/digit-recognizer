import torch
import torch.nn as nn
from torch.utils.data import random_split, DataLoader
from tqdm import tqdm
from dataset import Digit


# class Digit(Dataset):
#     def __init__(self, csv_path, transforms=None):
#         super().__init__()
#         self.csv_path = csv_path
#         self.transforms = transforms
#         self.db = pd.read_csv(self.csv_path)
#
#     def __len__(self):
#         return self.db.shape[0]
#
#     def __getitem__(self, index):
#         rv = self.db.iloc[index]
#         label = torch.tensor(rv['label'])
#         one_hot_label = nn.functional.one_hot(label, num_classes=10).to(torch.float)
#         img = rv.drop('label').to_numpy(dtype=np.float32).reshape((28, 28, 1))
#         img = img / 255.0
#         img = T.ToTensor()(img)
#         if self.transforms:
#             img = self.transforms(img)
#         return img, label


class Model(nn.Module):
    def __init__(self):
        super().__init__()

        # Convolution 1
        self.cnn1 = nn.Conv2d(in_channels=1, out_channels=16, kernel_size=5, stride=1, padding=0)
        self.relu1 = nn.ReLU()

        # Max pool 1
        self.maxpool1 = nn.MaxPool2d(kernel_size=2)

        # Convolution 2
        self.cnn2 = nn.Conv2d(in_channels=16, out_channels=32, kernel_size=5, stride=1, padding=0)
        self.relu2 = nn.ReLU()

        # Max pool 2
        self.maxpool2 = nn.MaxPool2d(kernel_size=2)

        # Fully connected 1
        self.fc1 = nn.Linear(32 * 4 * 4, 10)

    def forward(self, x):
        # Convolution 1
        out = self.cnn1(x)
        out = self.relu1(out)

        # Max pool 1
        out = self.maxpool1(out)

        # Convolution 2
        out = self.cnn2(out)
        out = self.relu2(out)

        # Max pool 2
        out = self.maxpool2(out)

        # flatten
        out = out.view(out.size(0), -1)

        # Linear function (readout)
        out = self.fc1(out)

        return out


batch_size = 100
dataset = Digit('data/train.csv')
train_ds, test_ds = random_split(dataset, [0.8, 0.2])
train_loader = DataLoader(train_ds, batch_size=batch_size)
test_loader = DataLoader(test_ds, batch_size=batch_size)


n_iters = 2500
num_epochs = n_iters / (len(dataset) / batch_size)
num_epochs = int(num_epochs)

# Create CNN
model = Model()

# Cross Entropy Loss
error = nn.CrossEntropyLoss()

# SGD Optimizer
learning_rate = 0.1
optimizer = torch.optim.SGD(model.parameters(), lr=learning_rate)


device = 'cuda' if torch.cuda.is_available() else 'cpu'

model.to(device)
error.to(device)

# CNN model training
count = 0
loss_list = []
iteration_list = []
accuracy_list = []
for epoch in range(num_epochs):
    for i, (images, labels) in tqdm(enumerate(train_loader)):

        train = images.to(device)
        # labels = Variable(labels)

        # Clear gradients
        optimizer.zero_grad()

        # Forward propagation
        outputs = model(train)

        # Calculate softmax and cross entropy loss
        loss = error(outputs, labels.to(device))

        # Calculating gradients
        loss.backward()

        # Update parameters
        optimizer.step()

        count += 1

        if count % 50 == 0:
            # print('Calculating Accuracy: ')
            # Calculate Accuracy
            correct = 0
            total = 0
            # Iterate through test dataset
            for images, labels in test_loader:
                test = images.to(device)

                # Forward propagation
                outputs = model(test)

                # Get predictions from the maximum value
                predicted = torch.max(outputs.data, 1)[1]

                # Total number of labels
                total += len(labels.to(device))

                correct += (predicted == labels.to(device)).sum()

            accuracy = 100 * correct / float(total)

            # store loss and iteration
            loss_list.append(loss.data.to('cpu'))
            iteration_list.append(count)
            accuracy_list.append(accuracy)
        if count % 100 == 0:
            # Print Loss
            print('Iteration: {}  Loss: {}  Accuracy: {} %'.format(count, loss.data, accuracy))
